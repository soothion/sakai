<?php
$timestamp = time();
$token = md5($timestamp . 'sakai-video');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" href="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/css/bootstrap.min.css">
            <link rel="stylesheet" href="css/jquery.fileupload.css">
                <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
                <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>  
                <style>
                    .input-group{margin-bottom: 20px;}
                </style>
                <title>sakia-api</title>
                </head>
                <body>
                    <div class="container">

                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#add" data-toggle="tab">添加</a></li>
                            <li><a href="#edit" data-toggle="tab">修改</a></li>
                            <li><a href="#delete" data-toggle="tab">删除</a></li>
                        </ul>
                        <div class="tab-content" style="margin-top:20px;">
                            <div class="tab-pane active" id="add">
                                <form role="form" action="http://202.120.83.180/phpcms/index.php?m=content&c=sakai&a=add" method="post">
                                    <div class="rows">   
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Select files...</span>
                                            <input id="fileupload" type="file" name="files[]" multiple>
                                        </span><!-- The global progress bar -->
                                        </br>
                                        </br>
                                        <div id="progress" class="progress">
                                            <div class="progress-bar progress-bar-success"></div>
                                        </div>
                                        <div id="files" class="files"></div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">视频</span>
                                        <input type="hidden" value="<?php echo $timestamp ?>" name="timestame"/>
                                        <input type="hidden" value="<?php echo $token ?>" name="token"/>
                                        <input type="text" class="form-control" name="video[local_video]" id="local_video" placeholder="视频路径">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">标题</span>
                                        <input type="text" class="form-control" name="video[title]" id="title" placeholder="标题">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">栏目</span>
                                        <input type="text" class="form-control" name="video[catid]" placeholder="视频简介">
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">画质</span>
                                        <input type="text" class="form-control" name="video[vision]" placeholder="1或者2">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">讲师</span>
                                        <input type="text" class="form-control" name="video[tea_name]" placeholder="讲师姓名">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">讲师简介</span>
                                        <input type="text" class="form-control" name="video[tea_des]" placeholder="讲师简介">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">内容</span>
                                        <textarea class="form-control" rows="8" name="video[content]"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                            <div class="tab-pane" id="edit">
                                <form role="form" action="http://202.120.83.180/phpcms/index.php?m=content&c=sakai&a=edit" method="post">
                                    <div class="input-group">
                                        <span class="input-group-addon">视频ID</span>
                                        <input type="text" class="form-control" name="video[id]" placeholder="视频ID">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">视频</span>
                                        <input type="hidden" value="<?php echo $timestamp ?>" name="timestame"/>
                                        <input type="hidden" value="<?php echo $token ?>" name="token"/>
                                        <input type="text" class="form-control" name="video[local_video]"  placeholder="视频路径">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">标题</span>
                                        <input type="text" class="form-control" name="video[title]" placeholder="标题">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">栏目</span>
                                        <input type="text" class="form-control" name="video[catid]" placeholder="视频简介">
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">画质</span>
                                        <input type="text" class="form-control" name="video[vision]" placeholder="1或者2">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">讲师</span>
                                        <input type="text" class="form-control" name="video[tea_name]" placeholder="讲师姓名">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">讲师简介</span>
                                        <input type="text" class="form-control" name="video[tea_des]" placeholder="讲师简介">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">内容</span>
                                        <textarea class="form-control" rows="8" name="video[content]"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                            <div class="tab-pane" id="delete">
                                <form role="form" action="http://202.120.83.180/phpcms/index.php?m=content&c=sakai&a=delete" method="post">
                                    <div class="input-group">
                                        <input type="hidden" value="<?php echo $timestamp ?>" name="timestame"/>
                                        <input type="hidden" value="<?php echo $token ?>" name="token"/>
                                        <span class="input-group-addon">视频ID</span>
                                        <input type="text" class="form-control" name="video[id]" placeholder="视频ID">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">栏目</span>
                                        <input type="text" class="form-control" name="video[catid]" placeholder="视频简介">
                                    </div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>

                        </div>
                    </div>

                    <script src="js/vendor/jquery.ui.widget.js"></script>
                    <script src="js/jquery.iframe-transport.js"></script>
                    <script src="js/jquery.fileupload.js"></script>
                    <script>
                        /*jslint unparam: true */
                        /*global window, $ */
                        $(function() {
                            'use strict';
                            var url = "http://202.120.83.180/phpcms/sakai.php?token=<?php echo $token ?>&timestamp=<?php echo $timestamp; ?>";
                            $('#fileupload').fileupload({
                                url: url,
                                dataType: 'json',
                                done: function(e, data) {
                                    $.each(data.result.files, function(index, file) {
                                        var local_video = $('#local_video').val();
                                        $('#local_video').val(local_video + ',' + file.url);
                                    });
                                },
                                progressall: function(e, data) {
                                    var progress = parseInt(data.loaded / data.total * 100, 10);
                                    $('#progress .progress-bar').css(
                                            'width',
                                            progress + '%'
                                            );
                                }
                            }).prop('disabled', !$.support.fileInput)
                                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
                        });
                    </script>
                </body>
                </html>
