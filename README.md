##接口列表
1.    添加
2.    编辑
3.    删除
    
##权限认证
所有调用请求必须添加以下两个参数：
1.    `timestamp` 当前时间戳
2.    `token` 权限认证码，其值为`md5(timestamp+'sakai-video')`

PHP示例
```
<?php 
    $timestamp = time();
    $token = md5($timestamp.'sakai-video');
?>
```

##调用方法
####1.添加

*    请求地址：index.php?m=content&c=sakai&a=add
*    使用jquery.fileupload上传插件上传视频
*    上传完成后会得到类似如下JSON数据

>{"files":[{"name":"demo.wmv","size":2909000,"type":"application\/x-msdownload","url":"uploadfile\/video\/org\/demo.wmv","deleteUrl":"http:\/\/bordon.xicp.net:8888\/phpcms\/?file=demo.wmv","deleteType":"DELETE"}]}


*    将其中的url解析出来，待用
*    请求参数列表

>
Item        | Type       |Null      |Des
---------   | -----      |-----     |
timestamp   | string     |0         |时间戳*  
token       | string     |0         |权限认证码*  
video[title]       | string     |0         |视频标题*  
video[catid]       | int        |0         |栏目ID，系统会新建一个EDX栏目，然后给定一个栏目ID*
video[local_videO] | string     |0         |视频路径，上传完视频后返回的URL*
video[vision]      | int        |1         |视频转码清晰度 1?2
video[tea_name]    | string     |1         |讲师姓名  
video[tea_des]     | string     |1         |讲师简介  
video[xuanduan]    | int        |1         |学段{1、2、3、4}分别对应{小学、初中、高中、大学}
video[xueke]       | int        |1         |学科{1、2、3、4、5、6、7、8}分别对应{语文、数学、英语、历史、地理、政治、物理、化学}
video[nianji]      | int        |1         |学段{1、2、3}分别对应{低年级、中年级、高年纪}
video[keywords]    | string     |1         |关键词
video[description] |string      |1         |课程简介
[content]     |string      |1         |课程内容


*    表单示例
```
        <form action="http://202.120.83.180/phpcms/index.php?m=content&c=sakai&a=add" method="post" id="form">
            <input type="hidden" name="token" value="a7379ecc56b4313b35a20dff1ad8664d"/>
            <input type="hidden" name="timestamp" value="1402992928"/>
            标题：<input type="text"   name="video[title]" value=""/></br>
            栏目：<input type="text"   name="video[catid]" value=""/></br>
            视频：<input type="text"   name="video[local_video]" value=""/></br>
            画质：<input type="text"   name="video[vision]" value=""/></br>
            讲师：<input type="text"   name="video[tea_name]" value=""/></br>
            讲师介绍：<input type="text"   name="video[tea_des]" value=""/></br>
            年级：<input type="text"   name="video[nianji]" value=""/></br>
            学科：<input type="text"   name="video[xueke]" value=""/></br>
            学段：<input type="text"   name="video[xueduan]" value=""/></br>
            <input type="submit" id="submit" value="submit"/>  
        </form>
```

*    返回参数列表(json数据)

Item        | Type              |Des
---------   | -----             |
id          | int               |视频ID  
status      | int               |状态，固定值99
purl        | string            |节目URL
thumb       | string            |生成的缩略图
localvideo  | string            |生成的MP4文件名：539ffb1b19a94.mp4，那么视频真实地址为：http://www.domain.com/uploadfile/video/539ffb1b19a94.mp4

        

####2.修改

*    请求地址：index.php?m=content&c=sakai&a=edit
*    请求参数列表

>
Item        | Type              |Des
---------   | -----             |
timestamp   | string             |时间戳  
token       | string              |权限认证码  
video[id]          | int                 |视频ID，上传完后返回的ID值  
video[catid]       | int                |栏目ID，视频所在栏目ID  
其他        | string            |参照添加请求参数列表  

*    表单示例
```
        <form action="http://202.120.83.180/phpcms/index.php?m=content&c=sakai&a=edit" method="post" id="form">
            <input type="hidden" name="token" value="a7379ecc56b4313b35a20dff1ad8664d"/>
            <input type="hidden" name="timestamp" value="1402992928"/>
            视频ID：<input type="text"   name="video[id]" value=""/></br>
            栏目：<input type="text"   name="video[catid]" value=""/></br>
            标题：<input type="text"   name="video[title]" value=""/></br>
            视频：<input type="text"   name="video[local_video]" value=""/></br>
            画质：<input type="text"   name="video[vision]" value=""/></br>
            讲师：<input type="text"   name="video[tea_name]" value=""/></br>
            讲师介绍：<input type="text"   name="video[tea_des]" value=""/></br>
            年级：<input type="text"   name="video[nianji]" value=""/></br>
            学科：<input type="text"   name="video[xueke]" value=""/></br>
            学段：<input type="text"   name="video[xueduan]" value=""/></br>
            <input type="submit" id="submit" value="submit"/>  
        </form>
```

*    成功则返回1，否则返回错误信息。


####3.删除

*    请求地址：index.php?m=content&c=sakai&a=delete
*    请求参数列表

>
Item        | Type              |Des
---------   | -----             |
timestamp   | string              |时间戳  
token       | string             |权限认证码  
video[id]          | int                 |视频ID，上传完后返回的ID值  
video[catid]       | int               |栏目ID，视频所在栏目ID  


*    表单示例
```
        <form action="http://202.120.83.180/phpcms/index.php?m=content&c=sakai&a=delete" method="post" id="form">
            <input type="hidden" name="token" value="a7379ecc56b4313b35a20dff1ad8664d"/>
            <input type="hidden" name="timestamp" value="1402992928"/>
            视频ID：<input type="text"   name="video[id]" value=""/></br>
            栏目：<input type="text"   name="video[catid]" value=""/></br>
            <input type="submit" id="submit" value="submit"/>  
        </form>
```       
        
*    成功则返回1，否则返回错误信息。
